package com.example.holo

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.PopupMenu
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.appcompat.widget.SwitchCompat
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.NavController
import androidx.navigation.fragment.NavHostFragment
import androidx.navigation.ui.setupWithNavController
import com.example.holo.data.model.Settings
import com.example.holo.ui.viewModel.ViewModel
import com.google.android.material.bottomnavigation.BottomNavigationView
import com.google.android.material.slider.Slider


class MainActivity : AppCompatActivity() {
    private lateinit var navController: NavController
    private lateinit var viewModel: ViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        supportActionBar!!.setDisplayShowCustomEnabled(true)
        supportActionBar!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))

        viewModel = ViewModelProvider(this)[ViewModel::class.java]
        viewModel.getSettings()

        val navHostFragment =
            supportFragmentManager.findFragmentById(R.id.nav_host_fragment) as NavHostFragment
        navController = navHostFragment.navController

        // la status bar canvia segons el fragment
        navController.addOnDestinationChangedListener { _, destination, _ ->
            when (destination.id) {
                R.id.liveFragment -> {
                    supportActionBar!!.setCustomView(R.layout.top_live_bar)

                    val tune = findViewById<View>(R.id.tune)

                    tune.setOnClickListener {
                        showSettingsDialog()
                    }
                }
                R.id.searchFragment -> {
                    supportActionBar!!.setCustomView(R.layout.top_search_bar)

                    val order = findViewById<View>(R.id.order)
                    val sortButton = findViewById<View>(R.id.sort_button)
                    sortButton.setOnClickListener { showPopup(sortButton) }

                    val toast = Toast.makeText(applicationContext, "", Toast.LENGTH_SHORT)

                    order.setOnClickListener {
                        toast.cancel()

                        if (viewModel.settings.value!!.order == "asc")
                            toast.setText("Order desc")
                        else
                            toast.setText("Order asc")
                        toast.show()

                        viewModel.changeOrder()
                    }
                }
                else -> {
                    supportActionBar!!.setCustomView(R.layout.top_bar)
                }
            }
        }
        val bottomNavigation = findViewById<BottomNavigationView>(R.id.bottom_navigation)

        bottomNavigation.setupWithNavController(navController)
    }

    private fun showPopup(view: View) {
        val popup = PopupMenu(this, view)
        popup.inflate(R.menu.sort_popup)
        popup.setOnMenuItemClickListener {
            when (it.title) {
                "Name" -> viewModel.settings.value!!.sorter = "name"
                "Subscribers" -> viewModel.settings.value!!.sorter = "subscriber_count"
                "Video Number" -> viewModel.settings.value!!.sorter = "video_count"
                "Time on YT" -> viewModel.settings.value!!.sorter = "published_at"
                "Total Views" -> viewModel.settings.value!!.sorter = "view_count"
            }

            viewModel.getChannels()
            //viewModel.saveSettings()

            return@setOnMenuItemClickListener false
        }
        popup.show()
    }

    private fun showSettingsDialog() {
        val dialog = Dialog(this)
        dialog.setContentView(R.layout.dialog_settings)
        dialog.window!!.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()

        val lookBackSlider = dialog.findViewById<Slider>(R.id.slider_lookback)
        val upcomingSlider = dialog.findViewById<Slider>(R.id.slider_upcoming)
        val switch = dialog.findViewById<SwitchCompat>(R.id.switch_fav)

        switch.isChecked = viewModel.settings.value!!.onlyFav

        lookBackSlider.value = viewModel.settings.value!!.lookBackHours.toFloat()
        upcomingSlider.value = viewModel.settings.value!!.maxUpcomingHours.toFloat()

        val goBackButton = dialog.findViewById<Button>(R.id.button_back)
        val applyButton = dialog.findViewById<Button>(R.id.button_apply)

        goBackButton.setOnClickListener {
            dialog.dismiss()
        }

        applyButton.setOnClickListener {
            viewModel.settings.value!!.lookBackHours = lookBackSlider.value.toInt()
            viewModel.settings.value!!.maxUpcomingHours = upcomingSlider.value.toInt()
            viewModel.settings.value!!.onlyFav = switch.isChecked

            //viewModel.saveSettings()
            viewModel.callStreams()
            dialog.dismiss()
        }
    }

    override fun onStop() {
        super.onStop()

        viewModel.saveSettings()

        println("Saved!!!")
    }

}