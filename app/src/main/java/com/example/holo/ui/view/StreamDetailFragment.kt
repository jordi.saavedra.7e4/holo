package com.example.holo.ui.view

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.navigation.findNavController
import com.bumptech.glide.Glide
import com.example.holo.R
import com.example.holo.ui.viewModel.ViewModel


class StreamDetailFragment:Fragment(R.layout.fragment_stream_detail) {
    lateinit var streamPhoto: ImageView
    lateinit var title: TextView
    lateinit var chImage: ImageView
    lateinit var chName: TextView
    lateinit var status: TextView
    lateinit var date: TextView
    lateinit var viewers: TextView
    lateinit var yt: Button

    private val viewModel: ViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        streamPhoto = view.findViewById(R.id.photo)
        title = view.findViewById(R.id.title)
        chImage = view.findViewById(R.id.channel_photo)
        chName = view.findViewById(R.id.channel_name)
        status = view.findViewById(R.id.status)
        date = view.findViewById(R.id.date)
        viewers = view.findViewById(R.id.views)
        yt = view.findViewById(R.id.yt)

        Glide.with(view).load(viewModel.currentStream.getThumbnailUrl()).into(streamPhoto)
        Glide.with(view).load(viewModel.currentStream.channel.photo).into(chImage)

        title.text = viewModel.currentStream.title
        chName.text = viewModel.currentStream.channel.name
        status.text = viewModel.currentStream.status
        date.text = viewModel.currentStream.setDate()
        viewers.text = viewModel.currentStream.live_viewers.toString()

        chImage.setOnClickListener {
            viewModel.callChannel(viewModel.currentStream.channel.id)
            view.findNavController().navigate(R.id.action_streamDetailFragment_to_chDetailFragment)
        }

        yt.setOnClickListener {
            val webIntent = Intent(Intent.ACTION_VIEW, Uri.parse(viewModel.currentStream.getStreamUrl()))
            startActivity(webIntent)
        }
    }
}