package com.example.holo.ui.view

import android.os.Bundle
import android.util.Log
import android.view.View
import android.widget.TextView
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.holo.R
import com.example.holo.data.model.Settings
import com.example.holo.ui.adapter.StreamAdapter
import com.example.holo.ui.viewModel.ViewModel

class LiveFragment : Fragment(R.layout.fragment_live) {
    private lateinit var liveRecyclerView: RecyclerView
    private lateinit var nextRecyclerView: RecyclerView
    private lateinit var recentRecyclerView: RecyclerView
    lateinit var liveText:TextView
    lateinit var upcomingText:TextView
    lateinit var recentText:TextView

    private val viewModel: ViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val adapterLive = StreamAdapter(viewModel)
        val adapterUpcoming = StreamAdapter(viewModel)
        val adapterRecent = StreamAdapter(viewModel)

        liveText = view.findViewById(R.id.live)
        upcomingText = view.findViewById(R.id.upcoming)
        recentText = view.findViewById(R.id.recent)

        viewModel.getFavorites()

        viewModel.settings.observe(viewLifecycleOwner) {
            viewModel.favorites.observe(viewLifecycleOwner){
                viewModel.callStreams()
            }
        }

        liveRecyclerView = view.findViewById(R.id.live_List)
        liveRecyclerView.layoutManager = LinearLayoutManager(this.requireContext(), LinearLayoutManager.HORIZONTAL, false)
        liveRecyclerView.adapter = adapterLive

        nextRecyclerView = view.findViewById(R.id.next_list)
        nextRecyclerView.layoutManager = LinearLayoutManager(this.requireContext(), LinearLayoutManager.HORIZONTAL, false)
        nextRecyclerView.adapter = adapterUpcoming

        recentRecyclerView = view.findViewById(R.id.recent_list)
        recentRecyclerView.layoutManager = LinearLayoutManager(this.requireContext(), LinearLayoutManager.HORIZONTAL, false)
        recentRecyclerView.adapter = adapterRecent

        viewModel.liveStreams.observe(viewLifecycleOwner) {
            liveText.text = "live"
            adapterLive.getStreams(it)
        }

        viewModel.upcomingStreams.observe(viewLifecycleOwner) {
            upcomingText.text = "upcoming"
            adapterUpcoming.getStreams(it)
        }

        viewModel.recentStreams.observe(viewLifecycleOwner) {
            recentText.text = "recent"
            adapterRecent.getStreams(it)
        }
    }
}