package com.example.holo.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.holo.R
import com.example.holo.data.model.Channel
import com.example.holo.ui.viewModel.ViewModel

class FavAdapter(private val viewModel: ViewModel) : RecyclerView.Adapter<FavAdapter.ChannelViewHolder>() {
    private var channels = mutableListOf<Channel>()

    fun setChannels(chList: MutableList<Channel>) {
        channels = chList
        notifyDataSetChanged()
    }

    fun deleteFromFavorites(pos: Int) {
        viewModel.unFav(channels[pos].id)
        channels.removeAt(pos)
        notifyDataSetChanged()
    }


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ChannelViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_fav, parent, false)
        return ChannelViewHolder(view)
    }

    override fun onBindViewHolder(holder: ChannelViewHolder, position: Int) {
        holder.bindData(channels[position])
    }

    override fun getItemCount(): Int {
        return channels.size
    }

    inner class ChannelViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var photo: ImageView = itemView.findViewById(R.id.channel_photo)
        private var name: TextView = itemView.findViewById(R.id.channel_name)

        fun bindData(ch: Channel) {
            Glide.with(itemView).load(ch.photo).into(photo)
            name.text = ch.name

            itemView.setOnClickListener {
                viewModel.callChannel(ch.id)
                itemView.findNavController().navigate(R.id.action_favFragment_to_chDetailFragment)
            }
        }
    }
}