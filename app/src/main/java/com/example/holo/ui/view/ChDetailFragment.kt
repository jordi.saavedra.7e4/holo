package com.example.holo.ui.view

import android.content.Intent
import android.net.Uri
import android.os.Bundle
import android.view.View
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import androidx.fragment.app.Fragment
import com.bumptech.glide.Glide
import com.example.holo.R
import com.example.holo.ui.viewModel.ViewModel
import androidx.fragment.app.activityViewModels
import android.os.Build

import androidx.annotation.RequiresApi

class ChDetailFragment:Fragment(R.layout.fragment_ch_detail){
    private lateinit var photo: ImageView
    private lateinit var name: TextView
    private lateinit var date: TextView
    private lateinit var subs: TextView
    private lateinit var videos: TextView
    private lateinit var fav:ImageView
    private lateinit var yt:Button
    private lateinit var twiter:Button

    private val viewModel: ViewModel by activityViewModels()

    @RequiresApi(Build.VERSION_CODES.R)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        photo = view.findViewById(R.id.channel_photo)
        name = view.findViewById(R.id.channel_name)
        date = view.findViewById(R.id.channel_date)
        subs = view.findViewById(R.id.channel_sub)
        videos = view.findViewById(R.id.channel_videos)
        fav = view.findViewById(R.id.fav)
        yt = view.findViewById(R.id.yt)
        twiter = view.findViewById(R.id.tw)

        viewModel.currentChannel.observe(viewLifecycleOwner) {
            it.setDate()
            name.text = it.name
            date.text = it.published_at
            subs.text = viewModel.getSubs()
            videos.text = it.video_count.toString()
            Glide.with(view).load(it.photo).into(photo)
            fav.alpha = viewModel.loadAlphaHeart()
        }

        fav.setOnClickListener {
            fav.alpha = viewModel.setAlphaHeart(fav.alpha)
        }

        yt.setOnClickListener {
            val webIntent = Intent(Intent.ACTION_VIEW, Uri.parse(viewModel.getChannelUrl()))
            startActivity(webIntent)
        }

        twiter.setOnClickListener {
            val webIntent = Intent(Intent.ACTION_VIEW, Uri.parse(viewModel.getTwitter()))
            startActivity(webIntent)
        }
    }
}