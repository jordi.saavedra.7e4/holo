package com.example.holo.ui.view

import android.os.Bundle
import android.view.View
import android.widget.PopupMenu
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.holo.R
import com.example.holo.ui.adapter.ChannelAdapter
import com.example.holo.ui.viewModel.ViewModel
import com.google.android.material.floatingactionbutton.FloatingActionButton

class SearchFragment : Fragment(R.layout.fragment_search) {
    lateinit var recyclerView: RecyclerView
    private val viewModel: ViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        viewModel.getChannels()

        val adapter = ChannelAdapter(viewModel)
        val grid = GridLayoutManager(this.requireContext(), 2)
        recyclerView = view.findViewById(R.id.channel_grid)
        recyclerView.layoutManager = grid
        recyclerView.adapter = adapter

        viewModel.channels.observe(viewLifecycleOwner) {
            adapter.setChannels(it)
        }
    }
}