package com.example.holo.ui.view

import android.os.Bundle
import android.view.View
import androidx.fragment.app.Fragment
import androidx.fragment.app.activityViewModels
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.holo.R
import com.example.holo.utils.SwipeToDelete
import com.example.holo.ui.viewModel.ViewModel
import com.example.holo.ui.adapter.FavAdapter

class FavFragment:Fragment(R.layout.fragment_fav) {
    lateinit var recyclerView: RecyclerView
    private val viewModel: ViewModel by activityViewModels()

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val adapter = FavAdapter(viewModel)

        recyclerView = view.findViewById(R.id.fav_list)
        recyclerView.layoutManager = LinearLayoutManager(this.requireContext())
        recyclerView.adapter = adapter

        viewModel.getFavorites()

        //SWIPE
        val swipeToDelete = object : SwipeToDelete(){
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.absoluteAdapterPosition
                adapter.deleteFromFavorites(position)
            }
        }

        val itemTouchHelper = ItemTouchHelper(swipeToDelete)
        itemTouchHelper.attachToRecyclerView(recyclerView)

        viewModel.favorites.observe(viewLifecycleOwner) {
            adapter.setChannels(it)
        }

    }
}