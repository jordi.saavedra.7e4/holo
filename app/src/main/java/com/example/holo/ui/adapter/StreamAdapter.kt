package com.example.holo.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import androidx.navigation.findNavController
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.Glide
import com.example.holo.R
import com.example.holo.data.model.Channel
import com.example.holo.data.model.Stream
import com.example.holo.ui.viewModel.ViewModel

class StreamAdapter(private val viewModel: ViewModel): RecyclerView.Adapter<StreamAdapter.StreamViewHolder>(){
    private var streamsList = mutableListOf<Stream>()

    fun getStreams(streams: List<Stream>){
        streamsList = streams.toMutableList()
        notifyDataSetChanged()
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StreamViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.item_stream, parent, false)
        return StreamViewHolder(view)
    }

    override fun onBindViewHolder(holder: StreamViewHolder, position: Int) {
        holder.bindData(streamsList[position])
    }

    override fun getItemCount(): Int {
        return streamsList.size
    }

    inner class StreamViewHolder(itemView: View) : RecyclerView.ViewHolder(itemView) {
        private var photo: ImageView = itemView.findViewById(R.id.photo)
        private var fav: ImageView = itemView.findViewById(R.id.fav_icon)
        fun bindData(stream: Stream) {
            fav.visibility = viewModel.isFav(stream.channel.id)

            Glide.with(itemView).load(stream.getThumbnailUrl()).into(photo)

            itemView.setOnClickListener {
                viewModel.setCurrentStream(stream.id, stream.status)
                itemView.findNavController().navigate(R.id.action_liveFragment_to_streamDetailFragment)
            }
        }
    }
}