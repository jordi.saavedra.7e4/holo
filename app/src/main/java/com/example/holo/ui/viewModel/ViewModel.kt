package com.example.holo.ui.viewModel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel

import android.util.Log
import android.view.View
import com.example.holo.data.model.*
import com.example.holo.data.repository.Repository
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class ViewModel : ViewModel() {
    private val repository = Repository()

    val channels = MutableLiveData<List<SimpleChannel>>()
    var currentChannel = MutableLiveData<Channel>()
    val favorites = MutableLiveData<MutableList<Channel>>()

    val liveStreams = MutableLiveData<List<Stream>>()
    val upcomingStreams = MutableLiveData<List<Stream>>()
    val recentStreams = MutableLiveData<List<Stream>>()
    lateinit var currentStream: Stream

    val settings = MutableLiveData<Settings>()

    fun saveSettings() {
        CoroutineScope(Dispatchers.Main).launch {
            withContext(Dispatchers.IO) {
                repository.saveSettings(settings.value!!)
                getSettings()
            }
        }
    }

    //crida els settings guardats al room
    fun getSettings() {
        CoroutineScope(Dispatchers.Main).launch {
            withContext(Dispatchers.IO) {
                settings.postValue(repository.getSettings())
            }
        }
    }

    //Canvia l'ordre (ascendent o descendent) dels canals
    fun changeOrder() {
        var order = settings.value!!.order
        order = if (order == "desc") "asc" else "desc"
        settings.value!!.order = order

        getChannels()
    }

    //CHANNELS
    fun getChannels() {
        val callCh = repository.getChannels(settings.value!!.sorter, settings.value!!.order)
        callCh.enqueue(object : Callback<ChannelCollection> {
            override fun onFailure(call: Call<ChannelCollection>, t: Throwable) {
                Log.e("ERROR", t.message.toString())
            }

            override fun onResponse(
                call: Call<ChannelCollection>,
                response: Response<ChannelCollection>
            ) {
                if (response.isSuccessful) {
                    channels.postValue(response.body()!!.channels)
                }
            }
        })
    }

    fun callChannel(id: Int) {
        val callCh = repository.getChannel(id)
        callCh.enqueue(object : Callback<Channel> {
            override fun onFailure(call: Call<Channel>, t: Throwable) {
                Log.e("ERROR", t.message.toString())
            }

            override fun onResponse(call: Call<Channel>, response: Response<Channel>) {
                if (response.isSuccessful) {
                    currentChannel.postValue(response.body())
                }
            }
        })
    }

    //STREAMS
    fun callStreams() {
        val call =
            repository.getStreams(settings.value!!.lookBackHours, settings.value!!.maxUpcomingHours)
        call.enqueue(object : Callback<StreamCollection> {
            override fun onFailure(call: Call<StreamCollection>, t: Throwable) {
                Log.e("ERROR", t.message.toString())
            }

            override fun onResponse(
                call: Call<StreamCollection>,
                response: Response<StreamCollection>
            ) {
                if (response.isSuccessful) {
                    val live: MutableList<Stream> = response.body()!!.live.toMutableList()
                    val upcoming: MutableList<Stream> = response.body()!!.upcoming.toMutableList()
                    val recent: MutableList<Stream> = response.body()!!.ended.toMutableList()

                    live.removeAt(0) //broken stream

                    if (settings.value!!.onlyFav) {
                        live.removeIf { stream -> !favorites.value!!.any { channel: Channel -> channel.id == stream.channel.id } }
                        upcoming.removeIf { stream -> !favorites.value!!.any { channel: Channel -> channel.id == stream.channel.id } }
                        recent.removeIf { stream -> !favorites.value!!.any { channel: Channel -> channel.id == stream.channel.id } }
                    } else {
                        live.sortByDescending { stream -> favorites.value!!.any { channel: Channel -> channel.id == stream.channel.id } }
                        upcoming.sortByDescending { stream -> favorites.value!!.any { channel: Channel -> channel.id == stream.channel.id } }
                        recent.sortByDescending { stream -> favorites.value!!.any { channel: Channel -> channel.id == stream.channel.id } }
                    }

                    liveStreams.postValue(live)
                    upcomingStreams.postValue(upcoming)
                    recentStreams.postValue(recent)
                }
            }
        })
    }

    //Busca el stream per a utilitzarlo a l'hora de veure'n els detalls
    fun setCurrentStream(id: Int, status: String) {
        when (status) {
            "live" -> currentStream = liveStreams.value!!.find { stream -> stream.id == id }!!
            "upcoming" -> currentStream =
                upcomingStreams.value!!.find { stream -> stream.id == id }!!
            "past" -> currentStream = recentStreams.value!!.find { stream -> stream.id == id }!!
        }
    }

    //Carrega l'opacitat segons si es favorit
    fun loadAlphaHeart(): Float {
        if (favorites.value!!.any { channel: Channel -> channel.id == currentChannel.value!!.id }) {
            return 1F
        }
        return 0.2F
    }

    //Canvia l'opacitat del cor segons si es favorit i actualitza la llista de favorits
    fun setAlphaHeart(alpha: Float): Float {
        println(currentChannel.value!!.id)
        return if (alpha == 1.0F) {
            println("Unfaving...")
            currentChannel.value?.id?.let { unFav(it) }
            0.2F
        } else {
            println("Faving...")
            favIt()
            1.0F
        }
    }

    //FAVORITS

    fun getFavorites() {
        CoroutineScope(Dispatchers.Main).launch {
            withContext(Dispatchers.IO) {
                favorites.postValue(repository.getFavorites())
                println("Loaded Favorites")
            }
        }
    }

    //Mostra el cor dels item channel si son favorits
    fun isFav(id: Int): Int {
        if (favorites.value!!.any { channel: Channel -> channel.id == id }) {
            return View.VISIBLE
        }
        return View.INVISIBLE
    }

    //Mètode per a treure un canal de favorits
    fun unFav(id: Int) {
        CoroutineScope(Dispatchers.Main).launch {
            withContext(Dispatchers.IO) {
                repository.unFav(id)
            }
        }
    }

    private fun favIt() {
        CoroutineScope(Dispatchers.Main).launch {
            withContext(Dispatchers.IO) {
                repository.addFav(currentChannel.value!!)
            }
        }

        getFavorites()  //Actualitza la llista de favorits del viewModel
    }

    //EXTRES

    fun getChannelUrl(): String {
        val channelId = currentChannel.value!!.yt_channel_id
        return "https://www.youtube.com/channel/$channelId"
    }

    fun getTwitter(): String {
        val twitterId = currentChannel.value!!.twitter_link
        return "https://www.twitter.com/$twitterId"
    }

    fun getSubs(): String {
        var subs = currentChannel.value!!.subscriber_count
        subs /= 1000
        if (subs < 1000)
            return "$subs K"
        return String.format("%.2f M", subs.toDouble() / 1000)
    }
}