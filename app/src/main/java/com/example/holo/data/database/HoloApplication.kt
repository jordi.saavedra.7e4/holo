package com.example.holo.data.database

import android.app.Application
import androidx.room.Room
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext


class HoloApplication : Application() {
    companion object {
        lateinit var holoDatabase: HoloDatabase
    }

    override fun onCreate() {
        super.onCreate()

        holoDatabase = Room.databaseBuilder(this, HoloDatabase::class.java, "HoloDatabase")
            .build()

        //Insereix Settings inicials al Room
        CoroutineScope(Dispatchers.Main).launch {
            withContext(Dispatchers.IO) { holoDatabase.populateInitialData() }
        }
    }
}