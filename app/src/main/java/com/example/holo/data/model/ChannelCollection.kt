package com.example.holo.data.model

data class ChannelCollection (
    val channels: List<SimpleChannel>
)