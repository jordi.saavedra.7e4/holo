package com.example.holo.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.example.holo.data.model.Channel

@Dao
interface ChannelDao {
    @Query("SELECT * FROM favorites")
    fun getFavorites(): MutableList<Channel>

    @Query("SELECT * FROM favorites WHERE id = :id")
    fun getFav(id: Int): Channel

    @Insert
    fun addFav(ch: Channel)

    @Query ("DELETE FROM favorites WHERE id = :id")
    fun unFav(id: Int)
}