package com.example.holo.data.api

import com.example.holo.data.model.*
import retrofit2.Call
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ApiService {
    companion object {
        private var retrofitService: ApiService? = null
        fun getInstance(): ApiService {
            if (retrofitService == null) {
                val retrofit = Retrofit.Builder()
                    .baseUrl("https://api.holotools.app/v1/")
                    .addConverterFactory(GsonConverterFactory.create())
                    .build()
                retrofitService = retrofit.create(ApiService::class.java)
            }
            return retrofitService!!
        }
    }

    /*S'afegeix el limit per que retorni 50 canals(el màxim, 25 per defecte), els ordena segons
    un atribut en ordre ascendent o descendent  */
    @GET("channels?limit=50")
    fun getChannels(
        @Query("sort") sort: String,
        @Query("order") order: String
    )
            : Call<ChannelCollection>

    //Calibrat per que retorni un número raonable d'items i no retorni estrenes ni streams que estan fixats per a dates molt distants
    @GET("live")
    fun getStreams(
        @Query("lookback_hours") lookback_hours: Int,
        @Query("max_upcoming_hours") max_upcoming_hours: Int
    ): Call<StreamCollection>

    @GET("channels/{id}")
    fun getChannel(@Path("id") id: Int): Call<Channel>

}