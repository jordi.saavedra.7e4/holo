package com.example.holo.data.database

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import androidx.room.Update
import com.example.holo.data.model.Settings

@Dao
interface SettingsDao {
    @Query("SELECT * FROM settings WHERE id = 0")
    fun getSettings(): Settings

    @Update
    fun updateSettings(settings: Settings)

    @Insert
    fun addInitialSetting(settings: Settings)

    @Query("SELECT COUNT(*) FROM settings")
    fun count(): Int

}