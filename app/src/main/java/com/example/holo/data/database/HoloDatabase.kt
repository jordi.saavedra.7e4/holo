package com.example.holo.data.database

import androidx.room.Database
import androidx.room.RoomDatabase
import com.example.holo.data.model.Channel
import com.example.holo.data.model.Settings


@Database(entities = [Channel::class, Settings::class], version = 1)
abstract class HoloDatabase : RoomDatabase() {
    abstract fun channelDao(): ChannelDao
    abstract fun settingsDao(): SettingsDao

     fun populateInitialData() {
        if (settingsDao().count() == 0) {
            runInTransaction {
               settingsDao().addInitialSetting(Settings(0,4, 12, "desc", "subscriber_count", false))
            }
        }
    }
}