package com.example.holo.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "favorites")
data class Channel(
    @PrimaryKey val id: Int,
    val name: String,
    var photo: String,
    var published_at: String,
    val subscriber_count: Int,
    val twitter_link: String,
    val video_count: Int,
    val view_count: Int,
    val yt_channel_id: String,
) {
    //Per a obtenir una data mes maca
    fun setDate(){
        published_at = published_at.split("T")[0]
    }

    override fun equals(other: Any?) = (other is Channel) || (other is SimpleChannel) && id == other.id
    override fun hashCode(): Int {
        var result = id
        result = 31 * result + name.hashCode()
        result = 31 * result + photo.hashCode()
        result = 31 * result + published_at.hashCode()
        result = 31 * result + subscriber_count
        result = 31 * result + twitter_link.hashCode()
        result = 31 * result + video_count
        result = 31 * result + view_count
        result = 31 * result + yt_channel_id.hashCode()
        return result
    }
}