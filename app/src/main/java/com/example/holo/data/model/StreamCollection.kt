package com.example.holo.data.model

data class StreamCollection(
    val ended: List<Stream>,
    val live: List<Stream>,
    val upcoming: List<Stream>
)