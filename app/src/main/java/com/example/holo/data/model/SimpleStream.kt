package com.example.holo.data.model

open class SimpleStream(
    open val channel: SimpleChannel,
    open val id: Int,
    open val yt_video_key: String,
    open val status:String
){
    fun getThumbnailUrl():String{return "https://img.youtube.com/vi/$yt_video_key/0.jpg"}
}