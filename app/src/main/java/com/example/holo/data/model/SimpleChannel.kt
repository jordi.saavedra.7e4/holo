package com.example.holo.data.model

data class SimpleChannel(
    val id: Int,
    val name: String,
    val photo: String,
)