package com.example.holo.data.model

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity (tableName = "settings")
data class Settings(
    @PrimaryKey val id: Int,
    var lookBackHours: Int,
    var maxUpcomingHours: Int,
    var order: String,
    var sorter: String,
    var onlyFav: Boolean
    )