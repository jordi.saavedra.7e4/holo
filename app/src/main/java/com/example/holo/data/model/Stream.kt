package com.example.holo.data.model

data class Stream(
    val channel: SimpleChannel,
    val id: Int,
    var live_start: String,
    var live_schedule:String,
    var live_end:String,
    val live_viewers: Int,
    val status: String,
    val title: String,
    val yt_video_key: String
) {

    //retorna la url on es troba la miniatura
    fun getThumbnailUrl(): String {
        return "https://img.youtube.com/vi/$yt_video_key/0.jpg"
    }

    //Per a obtenir només l'hora a la que comença un stream
    fun setDate(): String {
        var date = ""
        when (status) {
            "live" -> date = live_start
            "upcoming" -> date = live_schedule
            "past" -> date = live_end
        }
        date = date.split("T")[1]
        date = date.split(".")[0]
        return date
    }

    fun getStreamUrl(): String {
        return "https://www.youtube.com/watch?v=$yt_video_key"
    }
}