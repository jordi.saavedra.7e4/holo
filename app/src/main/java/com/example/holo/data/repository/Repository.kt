package com.example.holo.data.repository

import com.example.holo.data.api.ApiService
import com.example.holo.data.database.HoloApplication
import com.example.holo.data.model.Channel
import com.example.holo.data.model.Settings

class Repository {
    private val apiService: ApiService = ApiService.getInstance()
    private val channelDao = HoloApplication.holoDatabase.channelDao()
    private val settingsDao = HoloApplication.holoDatabase.settingsDao()

    fun getStreams(lookBackHours: Int, maxUpcomingHours: Int) =
        apiService.getStreams(lookBackHours, maxUpcomingHours)

    fun getFavorites() = channelDao.getFavorites()

    fun getChannels(sorter: String, order: String) = apiService.getChannels(sorter, order)

    fun getChannel(id: Int) = apiService.getChannel(id)

    fun addFav(channel: Channel) = channelDao.addFav(channel)

    fun unFav(id: Int) = channelDao.unFav(id)

    fun getSettings() = settingsDao.getSettings()

    fun saveSettings(settings: Settings) = settingsDao.updateSettings(settings)

}